# Changelog

This project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.1.0] - 2017-08-04

https://gitlab.com/Vinnl/wdio-webpack-dev-server-service/compare/v1.0.0...v1.1.0

### New features

- Mark as compatible with Webpack 3 in addition to Webpack 2 (to allow deduplication in dependent projects)

